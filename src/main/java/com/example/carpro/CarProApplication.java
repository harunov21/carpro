package com.example.carpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarProApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarProApplication.class, args);
    }

}
