package com.example.carpro.Controller;

import com.example.carpro.Model.Car;
import com.example.carpro.Service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("car")
public class CarController {
    private final CarService carService;

    @GetMapping
    public List<Car> getAllCars(){
       return carService.getAllCars();
    }
    @GetMapping("/{id}")
    public Optional<Car> findById (@PathVariable int id){
       return carService.findById(id);
    }
    @PostMapping("/{cars}")
    public Car saveCar(@RequestBody Car car){
        return carService.saveCar(car);
    }
    @PutMapping("/{id}")
    public Integer updateCar(@RequestBody Car car, @PathVariable int id) {
        return carService.updateCar(car, id);


    }
    @DeleteMapping("/{id}")
    public void deleteCar(@PathVariable int id){
        carService.delete(id);

    }
}
