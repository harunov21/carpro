package com.example.carpro.Service;

import com.example.carpro.Model.Car;

import java.util.List;
import java.util.Optional;

public interface CarService {
    List<Car> getAllCars();

    Optional<Car> findById(int id);

    Car saveCar(Car car);

    int updateCar(Car car, int id);

    void delete(int id);
}
