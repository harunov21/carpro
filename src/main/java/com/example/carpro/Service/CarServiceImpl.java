package com.example.carpro.Service;

import com.example.carpro.Model.Car;
import com.example.carpro.Repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService{
    private final CarRepository carRepository;
    @Override
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Override
    public Optional<Car> findById(int id) {
      return carRepository.findById(id);
    }

    @Override
    public Car saveCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public int updateCar(Car car, int id) {
        carRepository.findById(id).ifPresent(car1 -> {
            car1.setName(car.getName());
            car1.setColor(car.getColor());
            car1.setModel(car.getModel());
            car1.setEngineType(car.getEngineType());
            carRepository.save(car1);
        }); return id;
    }

    @Override
    public void delete(int id) {
        carRepository.deleteById(id);
    }


}
